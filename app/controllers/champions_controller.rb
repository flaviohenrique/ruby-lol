require 'lol'

class ChampionsController < ApplicationController

  # GET /champions
  # GET /champions.json
  def index
    client = Lol::Client.new ENV['LOL_KEY_API'] || "api-key", {region: "br"}
    # returns all attributes
    @champions = client.static.champion.get(champData: ['image', 'recommended'])
  end
end
